package first;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class BracesProblem {
    static boolean isBalanced(String input, Map<Character, Character> braces) {

        Stack<Character> balance = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            if (braces.containsValue(currentChar)) {
                balance.push(currentChar);
            } else if (braces.containsKey(currentChar)) {
                if (balance.isEmpty() || balance.pop() != braces.get(currentChar)) {
                    return false;
                }
            }
        }
        return balance.isEmpty();
    }

    public static void main(String[] args) {
        Map<Character, Character> braces = new HashMap<>();
        braces.put('}', '{');
        braces.put(']', '[');
        braces.put(')', '(');
        System.out.println(isBalanced("{[]}", braces));
    }
}
