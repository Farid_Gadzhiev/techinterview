package first;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MyLinkedList<T> {

    private Node<T> root;

    @Getter
    private int length;

    public void insert(T value) {
        Node<T> newNode = new Node<>(value);
        Node<T> currentNode = this.root;
        if (this.root != null) {
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = newNode;
        } else {
            this.root = newNode;
        }
        this.length++;
    }

    public void printList() {
        Node<T> currentNode = this.root;
        while (currentNode != null) {
            System.out.print(currentNode.value + " -> ");
            currentNode = currentNode.next;
        }
        System.out.println("X");
    }

    public static void main(String[] args) {
        MyLinkedList<Integer> test = new MyLinkedList<>();
        test.insert(1);
        test.insert(2);
        test.insert(3);
        test.insert(4);
        test.printList();
        System.out.println(test.getLength());
    }

    private static class Node<T> {
        T value;
        Node<T> next;

        Node(T value) {
            this.value = value;
        }
    }
}
