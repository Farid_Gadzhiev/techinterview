package first;

import lombok.AllArgsConstructor;

import java.util.Stack;

@AllArgsConstructor
public class OneQueueTwoStacks<T> {
    private Stack<T> in;
    private Stack<T> out;

    public void enqueue(T value) {
        in.push(value);
    }

    public T dequeue() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.pop();
    }
}