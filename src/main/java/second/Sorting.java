package second;

public class Sorting {

    public void quicksort(int[] array, int begin, int end) {
        if (begin >= end) {
            return;
        }
        int index = partition(array, begin, end);
        if (begin < index - 1) {
            quicksort(array, begin, index - 1);
        }
        if (index + 1 < end) {
            quicksort(array, index + 1, end);
        }
    }

    private int partition(int[] array, int begin, int end) {
        int leftIndex = begin;
        int rightIndex = end;
        int pivotIndex = array[(begin + end) / 2];
        while (leftIndex < rightIndex) {
            while (array[leftIndex] < pivotIndex) {
                leftIndex++;
                if (leftIndex == end) {
                    break;
                }
            }
            while (array[rightIndex] > pivotIndex) {
                rightIndex--;
                if (rightIndex == begin) {
                    break;
                }
            }
            if (leftIndex < rightIndex) {
                if (array[leftIndex] == array[rightIndex]) {
                    leftIndex++;
                } else {
                    swap(array, leftIndex, rightIndex);
                }
            }
        }
        return leftIndex;
    }

    private void swap(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }

    public void insertionSort(int[] arrayToInsertionSort) {
        for (int i = 1; i < arrayToInsertionSort.length; i++) {
            int currentNumber = arrayToInsertionSort[i];
            int j = i;
            while (j > 0 && currentNumber < arrayToInsertionSort[j - 1]) {
                arrayToInsertionSort[j] = arrayToInsertionSort[j - 1];
                j--;
            }
            arrayToInsertionSort[j] = currentNumber;
        }
    }


    public int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean isSorted = true;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                    isSorted = false;
                }
            }
            if (isSorted) {
                return array;
            }
        }
        return array;
    }

    public int[] mergeSort(int[] array) {
        if (array.length > 1) {
            int[] leftArray = new int[array.length / 2];
            int[] rightArray = new int[array.length - leftArray.length];

            System.arraycopy(array, 0, leftArray, 0, leftArray.length);
            System.arraycopy(array, leftArray.length, rightArray, 0, rightArray.length);

            leftArray = mergeSort(leftArray);
            rightArray = mergeSort(rightArray);

            merge(array, leftArray, rightArray);
        }
        return array;
    }

    private void merge(int[] destArray, int[] leftArray, int[] rightArray) {
        int destIndex = 0, leftArrayIndex = 0, rightArrayIndex = 0;
        while (leftArrayIndex != leftArray.length &&
                rightArrayIndex != rightArray.length) {
            if (leftArray[leftArrayIndex] < rightArray[rightArrayIndex]) {
                destArray[destIndex] = leftArray[leftArrayIndex];
                destIndex++;
                leftArrayIndex++;
            } else {
                destArray[destIndex] = rightArray[rightArrayIndex];
                destIndex++;
                rightArrayIndex++;
            }
        }

        for (int i = leftArrayIndex; i < leftArray.length; i++) {
            destArray[destIndex] = leftArray[i];
            destIndex++;
        }
        for (int i = rightArrayIndex; i < rightArray.length; i++) {
            destArray[destIndex] = rightArray[i];
            destIndex++;
        }
    }

    public void selectionSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }
}
